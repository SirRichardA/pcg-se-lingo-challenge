import sys
import random
import os
import re

# Get all the words with said length
def get_words_by_length(length):
    cache_file = "{0}_letters.txt".format(length)

    word_list = get_words_from_cache(cache_file)
    words = []

    # Line is initialized with a space, so it gets into the loop to begin with
    line = ' '

    while len(line) > 0:
        line = word_list.readline().rstrip()

        if len(line) == length:
            words.append(line)

    word_list.close()

    # Write words to cache if needed
    create_cache_file(cache_file, words)

    return words

# Get words from cache if possible, if not, get the whole thing
def get_words_from_cache(file_name):
    try:
        word_list = open("cache/{0}".format(file_name))
    except IOError:
        word_list = open('wordlist.txt')

    return word_list

# Cache the wordlist for faster loading
def create_cache_file(file_name, word_list):
    # Create the directory
    if not os.path.exists("cache"):
        os.makedirs("cache")

    # Create the file if not exists
    file_path = "cache/{0}".format(file_name)
    if not os.path.exists(file_path):
        file = open(file_path, "a+")
        file.write("\n".join(word_list))
        file.close()

# Check if the word is guessed
def is_word_correct(response):
    return re.search("/^O+$/", response);

def parse_response(response, guess):
    result = []

    for n in range(0, len(response)):
        guess_letter = guess[i]
        response_letter = response[i]

        if response_letter == "O":
            result.insert(i, guess_letter)

    return result

def filter_unsuitable_words(words, response_result):
    result_letters = list(response_result)

    for i, word in words:
        letters = list(word)

        if result_letters[0] == "X":
            words.remove(word);


length = int(sys.argv[1])

words = get_words_by_length(length)

for i in range(0, 5):
    index = random.randint(0, len(words) - 1)

    print(words[index], flush=True, file=sys.stdout)

    response = sys.stdin.readline()

    if is_word_correct(response):
        break

    words.remove(words[index])

    parse_response(response, words[index])
